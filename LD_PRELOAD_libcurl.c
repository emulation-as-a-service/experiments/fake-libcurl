// Copyright 2019 The Emulation-as-a-Service Authors
// SPDX-License-Identifier: CC0-1.0

#define _GNU_SOURCE
#include <curl/curl.h>
#include <dlfcn.h>
#include <stdarg.h>
#include <string.h>

int starts_with(const char *string, const char *prefix) {
  return !strncmp(string, prefix, strlen(prefix));
}

#define PRELOAD(sym, sym_t, filename)                        \
  static sym_t (*_##sym)();                                  \
  __attribute__((constructor)) static void _##sym##_init() { \
    _##sym = dlsym(dlopen(filename, RTLD_LAZY), #sym);       \
  }

// `curl_easy_setopt` is a macro in `curl/curl.h`
#undef curl_easy_setopt

PRELOAD(curl_easy_setopt, CURLcode, "libcurl.so.4");
CURLcode curl_easy_setopt(CURL *handle, CURLoption option, ...) {
  void *args = __builtin_apply_args();
  if (option == CURLOPT_URL) {
    va_list ap;
    va_start(ap, option);
    char *url = va_arg(ap, char *);
    va_end(ap);
    if (starts_with(url, "https://")) {
      char url2[4096];
      snprintf(url2, sizeof url2, "http://%s", url + strlen("https://"));
      return _curl_easy_setopt(handle, option, url2);
    }
  }
  __builtin_return(__builtin_apply((void *)_curl_easy_setopt, args, 64));
}
