all: LD_PRELOAD_libcurl.so example

# CFLAGS += -g
CFLAGS += -O2
CFLAGS += -Wall

LDLIBS += $(shell pkg-config --libs libcurl)

LD_PRELOAD_%.so: LD_PRELOAD_%.c
	$(CC) $(CFLAGS) -fPIC -shared -o $@ $< -ldl
	printf 'git-commit: %s\0' $(shell git describe --dirty --all --abbrev=128 --long --always) > .git-commit
	objcopy --add-section .note.git-commit=.git-commit $@
