#include <curl/curl.h>

int main() {
  CURL *curl = curl_easy_init();
  curl_easy_setopt(curl, CURLOPT_URL, "https://example.org");
  curl_easy_perform(curl);
}
